// Authentication middleware
const Authenticated = ({ redirect, store }) => {
  if (store.state.oauth.accessToken) {
    return redirect('/secret')
  }
}

export default Authenticated
